﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CForm : Form
    {
        #region WndProc Constants
        const int WM_NCLBUTTONDOWN = 0xA1;
        const int HTBORDER = 18;
        const int HTBOTTOM = 15;
        const int HTBOTTOMLEFT = 16;
        const int HTBOTTOMRIGHT = 17;
        const int HTCAPTION = 2;
        const int HTCLOSE = 20;
        const int HTGROWBOX = 4;
        const int HTLEFT = 10;
        const int HTMAXBUTTON = 9;
        const int HTMINBUTTON = 8;
        const int HTRIGHT = 11;
        const int HTSYSMENU = 3;
        const int HTTOP = 12;
        const int HTTOPLEFT = 13;
        const int HTTOPRIGHT = 14;
        #endregion

        private System.ComponentModel.IContainer components = null;

        private int captionHeight = 24;
        private Color borderColor;
        private int borderWidth = 4;
        protected CFormContent Content;
        private bool loaded;
        private Rectangle captionRect;
        private Rectangle iconRect;
        private Rectangle minimizeButtonRect;
        private Rectangle maximizeButtonRect;
        private Rectangle closeButtonRect;
        private Rectangle topLeftBorderRect;
        private Rectangle topRightBorderRect;
        private Rectangle topBorderRect;
        private Rectangle leftBorderRect;
        private Rectangle rightBorderRect;
        private Rectangle bottomLeftBorderRect;
        private Rectangle bottomRightBorderRect;
        private Rectangle bottomBorderRect;
        private bool drag = false;
        private int mousePosX = -1;
        private int mousePosY = -1;
        private bool mouseOverX = false;
        private bool mouseOverMaximize = false;
        private bool mouseOverMinimize = false;

#if DEBUG
        System.Diagnostics.Stopwatch stopWatch;
#endif

        public CForm()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            InitializeComponent();

            this.borderColor = this.BackColor;
#if DEBUG
            stopWatch = new System.Diagnostics.Stopwatch();
#endif
        }

        private void InitializeComponent()
        {
            this.Content = new CFormContent();
            this.SuspendLayout();
            // 
            // Content
            // 
            this.Content.BackColor = System.Drawing.Color.Black;
            this.Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Content.Location = new System.Drawing.Point(4, 28);
            this.Content.Name = "Content";
            this.Content.Size = new System.Drawing.Size(560, 362);
            this.Content.TabIndex = 0;
            this.Content.MouseEnter += new System.EventHandler(this.Content_MouseEnter);
            // 
            // CForm
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(568, 394);
            this.Controls.Add(this.Content);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CForm";
            this.Padding = new System.Windows.Forms.Padding(4, 28, 4, 4);
            this.ResumeLayout(false);

        }

        public Color BorderColor
        {
            get
            {
                return borderColor;
            }
            set
            {
                borderColor = value;
            }
        }

        public int BorderWidth
        {
            get
            {
                return borderWidth;
            }
            set
            {
                borderWidth = value;
            }
        }

        public int CaptionHeight
        {
            get
            {
                return captionHeight;
            }
            set
            {
                captionHeight = value;
            }
        }

        private void CalculateRects()
        {
            topLeftBorderRect = new Rectangle(0, 0, borderWidth, borderWidth);
            topBorderRect = new Rectangle(borderWidth, 0, ClientSize.Width - (borderWidth << 1), borderWidth);
            topRightBorderRect = new Rectangle(ClientSize.Width - borderWidth, 0, borderWidth, borderWidth);
            leftBorderRect = new Rectangle(0, borderWidth, borderWidth, ClientSize.Height - (borderWidth << 1));
            rightBorderRect = new Rectangle(ClientSize.Width - borderWidth, borderWidth, borderWidth, ClientSize.Height - (borderWidth << 1));
            bottomLeftBorderRect = new Rectangle(0, ClientSize.Height - borderWidth, borderWidth, borderWidth);
            bottomBorderRect = new Rectangle(borderWidth, ClientSize.Height - borderWidth, ClientSize.Width - (borderWidth << 1), borderWidth);
            bottomRightBorderRect = new Rectangle(ClientSize.Width - borderWidth, ClientSize.Height - borderWidth, borderWidth, borderWidth);

            captionRect = new Rectangle(borderWidth, borderWidth, ClientSize.Width - (borderWidth << 1), captionHeight);
            iconRect = new Rectangle(captionRect.Left + 3, captionRect.Top + 3, 16, 16);
            minimizeButtonRect = new Rectangle(captionRect.Right - 60, captionRect.Top + 3, 20, 16);
            maximizeButtonRect = new Rectangle(captionRect.Right - 40, captionRect.Top + 3, 20, 16);
            closeButtonRect = new Rectangle(captionRect.Right - 20, captionRect.Top + 3, 20, 16);
        }

        private Rectangle CloseButtonImageRect
        {
            get
            {
                return new Rectangle(40, 0, 20, 16);
            }
        }

        private Rectangle CloseButtonOverImageRect
        {
            get
            {
                return new Rectangle(100, 0, 20, 16);
            }
        }

        private Rectangle MaximizeButtonImageRect
        {
            get
            {
                return new Rectangle(20, 0, 20, 16);
            }
        }

        private Rectangle MaximizeButtonOverImageRect
        {
            get
            {
                return new Rectangle(80, 0, 20, 16);
            }
        }

        private Rectangle RestoreButtonImageRect
        {
            get
            {
                return new Rectangle(120, 0, 20, 16);
            }
        }

        private Rectangle RestoreButtonOverImageRect
        {
            get
            {
                return new Rectangle(140, 0, 20, 16);
            }
        }

        private Rectangle MinimizeButtonImageRect
        {
            get
            {
                return new Rectangle(0, 0, 20, 16);
            }
        }

        private Rectangle MinimizeButtonOverImageRect
        {
            get
            {
                return new Rectangle(60, 0, 20, 16);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            if ((levent.AffectedControl == this) && (levent.AffectedProperty == "Bounds"))
            {
                this.Padding = new Padding(borderWidth, borderWidth + captionHeight, borderWidth, borderWidth);
                CalculateRects();
            }
            base.OnLayout(levent);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
#if DEBUG
            stopWatch.Reset();
            stopWatch.Start();
#endif

            System.Diagnostics.Debug.WriteLine("OnPaint: ClipRectangle: {0}", new object[] { e.ClipRectangle });
            // Draw borders
            List<Rectangle> rects = new List<Rectangle>();

            if (e.ClipRectangle.IntersectsWith(topLeftBorderRect))
                rects.Add(topLeftBorderRect);

            if (e.ClipRectangle.IntersectsWith(topBorderRect))
                rects.Add(topBorderRect);

            if (e.ClipRectangle.IntersectsWith(topRightBorderRect))
                rects.Add(topRightBorderRect);

            if (e.ClipRectangle.IntersectsWith(leftBorderRect))
                rects.Add(leftBorderRect);

            if (e.ClipRectangle.IntersectsWith(rightBorderRect))
                rects.Add(rightBorderRect);

            if (e.ClipRectangle.IntersectsWith(bottomLeftBorderRect))
                rects.Add(bottomLeftBorderRect);

            if (e.ClipRectangle.IntersectsWith(bottomBorderRect))
                rects.Add(bottomBorderRect);

            if (e.ClipRectangle.IntersectsWith(bottomRightBorderRect))
                rects.Add(bottomRightBorderRect);

            if (rects.Count > 0)
            {
                using (SolidBrush borderBrush = new SolidBrush(borderColor))
                {
                    e.Graphics.FillRectangles(borderBrush, rects.ToArray());
                }
            }

            // Draw caption bar
            if (e.ClipRectangle.IntersectsWith(captionRect))
            {
                using (SolidBrush backBrush = new SolidBrush(this.BackColor))
                {
                    e.Graphics.FillRectangle(backBrush, captionRect);
                }
            }

            // Draw windows text
            if (!String.IsNullOrEmpty(this.Text))
            {
                using (SolidBrush textBrush = new SolidBrush(this.ForeColor))
                {
                    if ((this.ControlBox) && (this.ShowIcon))
                        e.Graphics.DrawString(this.Text, this.Font, textBrush, iconRect.Right + 3, iconRect.Top + 2);
                    else
                        e.Graphics.DrawString(this.Text, this.Font, textBrush, iconRect.Left + 3, iconRect.Top + 2);
                }
            }

            if (this.ControlBox)
            {
                // Draw icon
                if (this.ShowIcon)
                {
                    if (e.ClipRectangle.IntersectsWith(iconRect))
                    {
                        e.Graphics.DrawIcon(this.Icon, iconRect);
                    }
                }

                // Draw minimize button
                if (this.MinimizeBox)
                {
                    if (e.ClipRectangle.IntersectsWith(minimizeButtonRect))
                    {
                        if (mouseOverMinimize)
                            e.Graphics.DrawImage(CForm_Resources.form_buttons, minimizeButtonRect, MinimizeButtonOverImageRect, GraphicsUnit.Pixel);
                        else
                            e.Graphics.DrawImage(CForm_Resources.form_buttons, minimizeButtonRect, MinimizeButtonImageRect, GraphicsUnit.Pixel);
                    }
                }

                // Draw maximize/restore button
                if (this.MaximizeBox)
                {
                    if (e.ClipRectangle.IntersectsWith(maximizeButtonRect))
                    {
                        if (this.WindowState == FormWindowState.Maximized)
                        {
                            if (mouseOverMaximize)
                                e.Graphics.DrawImage(CForm_Resources.form_buttons, maximizeButtonRect, RestoreButtonOverImageRect, GraphicsUnit.Pixel);
                            else
                                e.Graphics.DrawImage(CForm_Resources.form_buttons, maximizeButtonRect, RestoreButtonImageRect, GraphicsUnit.Pixel);
                        }
                        else
                        {
                            if (mouseOverMaximize)
                                e.Graphics.DrawImage(CForm_Resources.form_buttons, maximizeButtonRect, MaximizeButtonOverImageRect, GraphicsUnit.Pixel);
                            else
                                e.Graphics.DrawImage(CForm_Resources.form_buttons, maximizeButtonRect, MaximizeButtonImageRect, GraphicsUnit.Pixel);
                        }
                    }
                }

                // Draw close button
                if (e.ClipRectangle.IntersectsWith(closeButtonRect))
                {
                    if (mouseOverX)
                        e.Graphics.DrawImage(CForm_Resources.form_buttons, closeButtonRect, CloseButtonOverImageRect, GraphicsUnit.Pixel);
                    else
                        e.Graphics.DrawImage(CForm_Resources.form_buttons, closeButtonRect, CloseButtonImageRect, GraphicsUnit.Pixel);
                }
            }

            if (!loaded)
                loaded = true;

#if DEBUG
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            System.Diagnostics.Debug.WriteLine("CForm draw time (ms) : {0}", ts.TotalMilliseconds);
#endif

            base.OnPaint(e);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void Content_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        protected override void OnStyleChanged(EventArgs e)
        {
            if (this.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None)
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            base.OnStyleChanged(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (this.ControlBox)
            {
                if (closeButtonRect.Contains(e.Location))
                {
                    if (!mouseOverX)
                    {
                        mouseOverX = true;
                        this.Invalidate(closeButtonRect);
                    }
                }
                else
                {
                    if (mouseOverX)
                    {
                        mouseOverX = false;
                        this.Invalidate(closeButtonRect);
                    }
                }

                if (this.MaximizeBox)
                {
                    if (maximizeButtonRect.Contains(e.Location))
                    {
                        if (!mouseOverMaximize)
                        {
                            mouseOverMaximize = true;
                            this.Invalidate(maximizeButtonRect);
                        }
                    }
                    else
                    {
                        if (mouseOverMaximize)
                        {
                            mouseOverMaximize = false;
                            this.Invalidate(maximizeButtonRect);
                        }
                    }
                }

                if (this.MinimizeBox)
                {
                    if (minimizeButtonRect.Contains(e.Location))
                    {
                        if (!mouseOverMinimize)
                        {
                            mouseOverMinimize = true;
                            this.Invalidate(minimizeButtonRect);
                        }
                    }
                    else
                    {
                        if (mouseOverMinimize)
                        {
                            mouseOverMinimize = false;
                            this.Invalidate(minimizeButtonRect);
                        }
                    }
                }
            }

            if (this.WindowState == FormWindowState.Normal)
            {
                this.Cursor = Cursors.Default;
                if (topBorderRect.Contains(e.Location) || bottomBorderRect.Contains(e.Location))
                {
                    this.Cursor = Cursors.SizeNS;
                }
                else if (leftBorderRect.Contains(e.Location) || rightBorderRect.Contains(e.Location))
                {
                    this.Cursor = Cursors.SizeWE;
                }
                else if (topLeftBorderRect.Contains(e.Location) || bottomRightBorderRect.Contains(e.Location))
                {
                    this.Cursor = Cursors.SizeNWSE;
                }
                else if (topRightBorderRect.Contains(e.Location) || bottomLeftBorderRect.Contains(e.Location))
                {
                    this.Cursor = Cursors.SizeNESW;
                }

                if (drag)
                {
                    this.Top = Cursor.Position.Y - mousePosY;
                    this.Left = Cursor.Position.X - mousePosX;
                }
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            drag = false;
            base.OnMouseUp(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (this.ControlBox)
            {
                if (closeButtonRect.Contains(e.Location))
                {
                    this.Close();
                    return;
                }

                if (this.MinimizeBox)
                {
                    if (minimizeButtonRect.Contains(e.Location))
                    {
                        this.WindowState = FormWindowState.Minimized;
                        return;
                    }
                }

                if (this.MaximizeBox)
                {
                    if (maximizeButtonRect.Contains(e.Location))
                    {
                        if (WindowState == FormWindowState.Normal)
                        {
                            WindowState = FormWindowState.Maximized;
                        }
                        else
                        {
                            WindowState = FormWindowState.Normal;
                        }
                        this.Refresh();
                        return;
                    }
                }
            }

            if (captionRect.Contains(e.Location))
            {
                drag = true;
                mousePosX = Cursor.Position.X - this.Left;
                mousePosY = Cursor.Position.Y - this.Top;
                return;
            }

            if (this.WindowState == FormWindowState.Normal)
            {
                if (e.Button == MouseButtons.Left)
                {
                    this.Capture = false;
                    Cursor cursor = Cursors.Arrow;
                    IntPtr direction = new IntPtr(Bottom);

                    if (topLeftBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTTOPLEFT;
                        cursor = Cursors.SizeNWSE;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (topBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTTOP;
                        cursor = Cursors.SizeNS;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (topRightBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTTOPRIGHT;
                        cursor = Cursors.SizeNESW;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (leftBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTLEFT;
                        cursor = Cursors.SizeWE;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (rightBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTRIGHT;
                        cursor = Cursors.SizeWE;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (bottomLeftBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTBOTTOMLEFT;
                        cursor = Cursors.SizeNESW;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (bottomBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTBOTTOM;
                        cursor = Cursors.SizeNS;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                    if (bottomRightBorderRect.Contains(e.Location))
                    {
                        direction = (IntPtr)HTBOTTOMRIGHT;
                        cursor = Cursors.SizeNWSE;
                        this.Cursor = cursor;
                        Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, direction, IntPtr.Zero);
                        DefWndProc(ref msg);
                        return;
                    }
                }
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (captionRect.Contains(e.Location))
                {
                    if (WindowState == FormWindowState.Normal)
                    {
                        WindowState = FormWindowState.Maximized;
                    }
                    else
                    {
                        WindowState = FormWindowState.Normal;
                    }
                    this.Refresh();
                }
            }
            base.OnMouseDoubleClick(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            if (mouseOverX)
            {
                mouseOverX = false;
                this.Invalidate(closeButtonRect);
            }
            if (mouseOverMinimize)
            {
                mouseOverMinimize = false;
                this.Invalidate(minimizeButtonRect);
            }
            if (mouseOverMaximize)
            {
                mouseOverMaximize = false;
                this.Invalidate(maximizeButtonRect);
            }
            this.Cursor = Cursors.Arrow;
            base.OnMouseLeave(e);
        }
    }
}
