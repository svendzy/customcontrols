﻿using System;

namespace CustomControls
{
    public class CVScrollEventArgs : System.EventArgs
    {
        private int pos;

        public CVScrollEventArgs() : base()
        {
        }

        public int Pos
        {
            get { return pos; }
            set { pos = value; }
        }
    }
}
