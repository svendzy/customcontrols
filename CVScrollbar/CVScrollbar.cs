﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CVScrollbar : Control
    {
        private int autoScrollMin;
        private int pos;
        private int max;
        private int smallChange;
        private int thumbMinHeight;
        private Rectangle upArrowRect;
        private Rectangle downArrowRect;
        private Rectangle thumbContainerRect;
        private Rectangle thumbRect;
        private Timer scrollTimer;
        private enum ScrollDir { Up, Down };
        private ScrollDir scrollDir;
        private bool loaded = false;
        private bool reCalcScrollbarBounds = true;
        private bool reCalcThumbBounds = true;
        public delegate void DnVScrollEventHandler(object sender, CVScrollEventArgs e);
        public event DnVScrollEventHandler Scroll;

        public CVScrollbar()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.scrollTimer = new Timer();
            this.scrollTimer.Enabled = false;
            this.scrollTimer.Interval = 30;
            this.scrollTimer.Tick += new EventHandler(scrollTimer_Tick);
            this.scrollDir = ScrollDir.Down;

            this.autoScrollMin = 0;
            this.max = 0;
            this.pos = 0;
            this.smallChange = 5;
            this.thumbMinHeight = 15;
        }

        protected override Size DefaultSize
        {
            get
            {
                return new Size(16, 200);
            }
        }

        protected void OnScroll(object sender, CVScrollEventArgs e)
        {
            if (Scroll != null)
            {
                Scroll(this, e);
            }
        }

        public int AutoScrollMin
        {
            get
            {
                return autoScrollMin;
            }
            set
            {
                autoScrollMin = value;
                if (loaded)
                {
                    reCalcThumbBounds = true;
                    this.Refresh();
                }
            }
        }

        public int ThumbMinHeight
        {
            get
            {
                return thumbMinHeight;
            }
            set
            {
                thumbMinHeight = value;
                if (loaded)
                {
                    reCalcThumbBounds = true;
                    this.Refresh();
                }
            }
        }

        public int Pos
        {
            get
            {
                return pos;
            }
            set
            {
                if (pos != value)
                {
                    if (value < 0)
                        pos = 0;
                    else
                    {
                        if (value <= max)
                            pos = value;
                        else
                            pos = max;
                    }
                    if (loaded)
                    {
                        reCalcThumbBounds = true;
                        this.Refresh();
                    }
                }
            }
        }

        public int Max
        {
            get
            {
                return max;
            }
            set
            {
                if (max != value)
                {
                    if (value < pos)
                        pos = value;
                    max = value;
                    if (loaded)
                    {
                        reCalcThumbBounds = true;
                        this.Refresh();
                    }
                }
            }
        }

        public int SmallChange
        {
            get
            {
                return smallChange;
            }
            set
            {
                smallChange = value;
            }
        }

        public int ScrollInterval
        {
            get
            {
                return scrollTimer.Interval;
            }
            set
            {
                scrollTimer.Interval = value;
            }
        }

        private void CalculateScrollbarBounds()
        {
            this.upArrowRect = new Rectangle(0, 0, 16, 16);
            this.thumbContainerRect = new Rectangle(0, upArrowRect.Height, this.ClientSize.Width, this.ClientSize.Height - 32);
            this.downArrowRect = new Rectangle(0, this.ClientRectangle.Height - 16, 16, 16);
        }

        private void CalculateThumbBounds()
        {
            int contentHeight = this.ClientSize.Height + this.max;
            float scrollRatio = (float)this.ClientSize.Height / (float)contentHeight;
            float thumbHeight = (float)thumbContainerRect.Height * scrollRatio;

            float thumbScrollHeight = (float)thumbContainerRect.Height - thumbHeight;
            float thumbOffsetY = ((float)this.pos / (float)this.max) * thumbScrollHeight;
            this.thumbRect = new Rectangle(0, 16 + (int)thumbOffsetY, this.thumbContainerRect.Width, (int)thumbHeight);
            this.thumbRect.Inflate(-2, 0);
        }

        void scrollTimer_Tick(object sender, EventArgs e)
        {
            switch (scrollDir)
            {
                case ScrollDir.Up:
                    if (Pos > 0)
                    {
                        CVScrollEventArgs args = new CVScrollEventArgs();
                        if (Pos >= smallChange)
                            Pos -= smallChange;
                        else
                            Pos = 0;
                        args.Pos = Pos;
                        OnScroll(this, args);
                    }
                    break;
                case ScrollDir.Down:
                    if (Pos < max)
                    {
                        CVScrollEventArgs args = new CVScrollEventArgs();
                        if (Pos <= max - smallChange)
                            Pos += smallChange;
                        else
                            Pos = max;
                        args.Pos = Pos;
                        OnScroll(this, args);
                    }
                    break;
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (upArrowRect.Contains(e.Location))
                {
                    scrollDir = ScrollDir.Up;
                    scrollTimer.Enabled = true;
                }
                else if (downArrowRect.Contains(e.Location))
                {
                    scrollDir = ScrollDir.Down;
                    scrollTimer.Enabled = true;
                }
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                scrollTimer.Enabled = false;
            }
            base.OnMouseUp(e);
        }

        protected override void OnResize(EventArgs e)
        {
            reCalcScrollbarBounds = true;
            reCalcThumbBounds = true;
            this.Refresh();
            base.OnResize(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        private void DrawThumb(Rectangle newThumbRect, Rectangle oldThumbRect)
        {
            using (Graphics graphics = this.CreateGraphics())
            {
                using (SolidBrush eraseBrush = new SolidBrush(this.BackColor))
                {
                    graphics.FillRectangle(eraseBrush, oldThumbRect);
                }
                graphics.FillRectangle(Brushes.Black, newThumbRect);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            // Set Scrollbar BackColor
            e.Graphics.Clear(this.BackColor);

            if (reCalcScrollbarBounds)
                this.CalculateScrollbarBounds();
            if (reCalcThumbBounds)
                this.CalculateThumbBounds();

            // Draw Up arrow for small change
            if (e.ClipRectangle.IntersectsWith(upArrowRect))
                e.Graphics.DrawImage(Resources.triangle_scrollbar_up_wt, upArrowRect);

            // Draw Scrollbar Thumb
            if ((thumbRect != Rectangle.Empty) && (e.ClipRectangle.IntersectsWith(thumbRect)))
            {
                using (SolidBrush thumbBrush = new SolidBrush(this.ForeColor))
                {
                    e.Graphics.FillRectangle(thumbBrush, thumbRect);
                }
            }

            // Draw Up arrow for small change
            if (e.ClipRectangle.IntersectsWith(downArrowRect))
                e.Graphics.DrawImage(Resources.triangle_scrollbar_down_wt, downArrowRect);

            if (!loaded)
                loaded = true;

            base.OnPaint(e);
        }

        protected override void Dispose(bool disposing)
        {
            this.scrollTimer.Tick -= scrollTimer_Tick;
            this.scrollTimer.Dispose();
            base.Dispose(disposing);
        }
    }
}
