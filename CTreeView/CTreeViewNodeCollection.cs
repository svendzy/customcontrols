﻿using System;
using System.Collections.Generic;

namespace CustomControls
{
    public class CTreeViewNodeCollection : IList<CTreeViewNode>
    {
        private List<CTreeViewNode> items;
        private CTreeViewNode owner;
        private CTreeView treeView;

        public CTreeViewNodeCollection(CTreeViewNode owner, CTreeView treeView = null)
        {
            this.owner = owner;
            this.items = new List<CTreeViewNode>();
            this.treeView = treeView;
        }

        public int IndexOf(CTreeViewNode item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, CTreeViewNode item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public CTreeViewNode this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                items[index] = value;
            }
        }

        public void Add(CTreeViewNode item)
        {
            item.Parent = owner;
            items.Add(item);
            if ((treeView != null) && (!treeView.Updating))
                treeView.NodeContainer.Refresh();
        }

        public void Clear()
        {
            items.Clear();
            if ((treeView != null) && (!treeView.Updating))
                treeView.NodeContainer.Refresh();
        }

        public bool Contains(CTreeViewNode item)
        {
            return items.Contains(item);
        }

        public void CopyTo(CTreeViewNode[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get 
            {
                return items.Count;
            }
        }

        public bool IsReadOnly
        {
            get 
            {
                return false;
            }
        }

        public bool Remove(CTreeViewNode item)
        {
            bool  bRemoved = items.Remove(item);
            if (bRemoved)
            {
                if ((treeView != null) && (!treeView.Updating))
                    treeView.NodeContainer.Refresh();
            }
            return bRemoved;
        }

        public IEnumerator<CTreeViewNode> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
