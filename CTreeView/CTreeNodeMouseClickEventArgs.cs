﻿using System;

namespace CustomControls
{
    public delegate void DnTreeNodeMouseClickEventHandler(object sender, CTreeNodeMouseClickEventArgs args);

    public class CTreeNodeMouseClickEventArgs : System.EventArgs
    {
        private CTreeViewNode node;

        public CTreeViewNode Node 
        {
            get
            {
                return node;
            }
            set
            {
                node = value;
            }
        }
    }
}
