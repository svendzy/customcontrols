﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace CustomControls
{
    public class CTreeView : Control
    {
        private const int DEFAULT_HEADER_HEIGHT = 20;
        private const int DEFAULT_INDENT = 5;
        private const int DEFAULT_NODE_HEIGHT = 20;

        private int headerHeight = DEFAULT_HEADER_HEIGHT;
        private int indent = DEFAULT_INDENT;
        private System.Windows.Forms.ImageList imageList;
        private int nodeHeight = DEFAULT_NODE_HEIGHT;
        private string headerText = "Default header text";
        private Color headerGradientStartColor;
        private Color headerGradientEndColor;
        private CTreeViewNodeContainer nodeContainer;
        private bool updating = false;
        private bool bLoaded = false;
        public event DnTreeNodeMouseClickEventHandler NodeMouseClick;

        public CTreeView()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.SuspendLayout();

            CVScrollbar scrollBar = new CVScrollbar();
            scrollBar.Location = new Point(this.ClientSize.Width - 16, headerHeight);
            scrollBar.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
            scrollBar.Size = new Size(16, this.ClientSize.Height - headerHeight);
            scrollBar.AutoScrollMin = this.ClientSize.Height - headerHeight;
            scrollBar.Max = scrollBar.AutoScrollMin;
            scrollBar.SmallChange = nodeHeight;

            this.nodeContainer = new CTreeViewNodeContainer(this);
            this.nodeContainer.Location = new Point(0, headerHeight);
            this.nodeContainer.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            this.nodeContainer.Size = new Size(this.ClientSize.Width - 16, this.ClientSize.Height - headerHeight);
            this.nodeContainer.ScrollBar = scrollBar;
            this.nodeContainer.NodeMouseClick += new DnTreeNodeMouseClickEventHandler(nodeContainer_NodeMouseClick);

            this.Theme = CTreeViewTheme.Dark;

            this.Controls.Add(this.nodeContainer);
            this.Controls.Add(scrollBar);
            this.ResumeLayout();
        }

        protected override Size DefaultSize
        {
            get
            {
                return new Size(100, 200);
            }
        }

        public int HeaderHeight
        {
            get
            {
                return headerHeight;
            }
            set
            {
                headerHeight = value;
            }
        }

        public string HeaderText
        {
            get
            {
                return headerText;
            }
            set
            {
                headerText = value;
            }
        }

        public Color HeaderGradientStartColor
        {
            get
            {
                return headerGradientStartColor;
            }
            set
            {
                headerGradientStartColor = value;
            }
        }

        public Color HeaderGradientEndColor
        {
            get
            {
                return headerGradientEndColor;
            }
            set
            {
                headerGradientEndColor = value;
            }
        }

        public int NodeHeight
        {
            get
            {
                return nodeHeight;
            }
            set
            {
                nodeHeight = value;
                nodeContainer.ScrollBar.SmallChange = nodeHeight;
            }
        }

        public int Indent
        {
            get
            {
                return indent;
            }
            set
            {
                indent = value;
            }
        }

        public ImageList ImageList
        {
            get
            {
                return imageList;
            }
            set
            {
                imageList = value;
            }
        }

        public CTreeViewNodeCollection Nodes
        {
            get
            {
                return nodeContainer.Nodes;
            }
        }

        internal CTreeViewNodeContainer NodeContainer
        {
            get
            {
                return nodeContainer;
            }
        }

        internal bool Updating
        {
            get
            {
                return updating;
            }
        }

        public CTreeViewTheme Theme
        {
            set
            {
                switch (value)
                {
                    case CTreeViewTheme.Dark:
                        this.headerGradientStartColor = Color.FromArgb(33, 33, 33);
                        this.headerGradientEndColor = Color.Black;
                        this.BackColor = Color.FromArgb(23, 24, 27);
                        this.ForeColor = Color.White;
                        this.nodeContainer.BackColor = this.BackColor;
                        this.nodeContainer.ForeColor = this.ForeColor;
                        this.nodeContainer.ScrollBar.BackColor = Color.FromArgb(48, 51, 60);
                        this.nodeContainer.ScrollBar.ForeColor = Color.FromArgb(131, 136, 140);
                        break;
                    case CTreeViewTheme.Light:
                        this.headerGradientStartColor = Color.FromArgb(248, 248, 248);
                        this.headerGradientEndColor = Color.White;
                        this.BackColor = Color.FromArgb(250, 250, 250);
                        this.ForeColor = Color.Black;
                        this.nodeContainer.BackColor = this.BackColor;
                        this.nodeContainer.ForeColor = this.ForeColor;
                        this.nodeContainer.ScrollBar.BackColor = Color.FromArgb(209, 209, 209);
                        this.nodeContainer.ScrollBar.ForeColor = Color.FromArgb(0, 143, 0);
                        break;
                }
            }
        }

        void nodeContainer_NodeMouseClick(object sender, CTreeNodeMouseClickEventArgs args)
        {
            OnNodeMouseClick(this, args);
        }

        protected void OnNodeMouseClick(object sender, CTreeNodeMouseClickEventArgs e)
        {
            if (NodeMouseClick != null)
            {
                NodeMouseClick(sender, e);
            }
        }

        public override void Refresh()
        {
            base.Refresh();
            this.nodeContainer.Refresh();
        }

        public void BeginUpdate()
        {
            updating = true;
        }

        public void EndUpdate()
        {
            updating = false;
            this.Refresh();
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            if ((levent.AffectedControl == this) && (levent.AffectedProperty == "Bounds"))
            {
                this.nodeContainer.Location = new Point(0, headerHeight);
                this.nodeContainer.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
                this.nodeContainer.Size = new Size(this.ClientSize.Width - 16, this.ClientSize.Height - headerHeight);
                this.nodeContainer.ScrollBar.Location = new Point(this.ClientSize.Width - 16, headerHeight);
                this.nodeContainer.ScrollBar.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
                this.nodeContainer.ScrollBar.Size = new Size(16, this.ClientSize.Height - headerHeight);
                this.nodeContainer.ScrollBar.AutoScrollMin = this.nodeContainer.ClientSize.Height;
            }
            base.OnLayout(levent);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle headerRect = new Rectangle(0, 0, this.ClientSize.Width, headerHeight);
            using (LinearGradientBrush headerBrush = new LinearGradientBrush(headerRect, headerGradientStartColor, headerGradientEndColor, 90F))
            {
                e.Graphics.FillRectangle(headerBrush, headerRect);
                ControlPaint.DrawBorder(e.Graphics, headerRect, Color.White, 1, ButtonBorderStyle.Outset,
                                Color.White, 1, ButtonBorderStyle.Outset, Color.White, 1, ButtonBorderStyle.Outset,
                                Color.White, 1, ButtonBorderStyle.Outset);
                TextRenderer.DrawText(e.Graphics, this.headerText, this.Font, headerRect, this.ForeColor);
            }

            if (!bLoaded)
                bLoaded = true;
            base.OnPaint(e);
        }

        protected override void Dispose(bool disposing)
        {
            nodeContainer.NodeMouseClick -= nodeContainer_NodeMouseClick;
            base.Dispose(disposing);
        }
    }
}
