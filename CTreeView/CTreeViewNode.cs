﻿using System;
using System.Drawing;

namespace CustomControls
{
    public class CTreeViewNode
    {
        private string name;
        private Color foreColor;
        private string text;
        private string tooltipText;
        private int imageIndex;
        private string imageKey;
        private object tag;
        private bool expanded;
        private CTreeViewNodeCollection nodes;
        private Rectangle bounds;
        private CTreeViewNode parent;

        public CTreeViewNode()
        {
            this.name = String.Empty;
            this.foreColor = Color.Empty;
            this.text = String.Empty;
            this.tooltipText = String.Empty;
            this.imageIndex = -1;
            this.imageKey = String.Empty;
            this.tag = null;
            this.expanded = false;
            this.nodes = new CTreeViewNodeCollection(this);
            this.bounds = Rectangle.Empty;
            this.parent = null;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Color ForeColor
        {
            get { return foreColor; }
            set { foreColor = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public string TooltipText
        {
            get { return tooltipText; }
            set { tooltipText = value; }
        }

        public int ImageIndex
        {
            get { return imageIndex; }
            set { imageIndex = value; }
        }

        public string ImageKey
        {
            get { return imageKey; }
            set { imageKey = value; }
        }

        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        public bool Expanded
        {
            get { return expanded; }
            set { expanded = value; }
        }

        public CTreeViewNodeCollection Nodes
        {
            get
            {
                return nodes;
            }
        }

        public Rectangle Bounds 
        {
            get
            {
                return bounds;
            }
            internal set
            {
                bounds = value;
            }
        }

        public CTreeViewNode Parent
        {
            get
            {
                return parent;
            }
            internal set
            {
                parent = value;
            }
        }
    }
}
