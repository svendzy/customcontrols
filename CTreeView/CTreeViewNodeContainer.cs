﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CTreeViewNodeContainer : Control
    {
        private CTreeViewNodeCollection nodes;
        private CTreeView treeView;
        private CTreeViewNode firstDisplayedNode;
        private CVScrollbar scrollBar;
        public event DnTreeNodeMouseClickEventHandler NodeMouseClick;

        public CTreeViewNodeContainer(CTreeView treeView)
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.nodes = new CTreeViewNodeCollection(null, treeView);
            this.treeView = treeView;
            this.firstDisplayedNode = null;
        }

        internal CTreeView TreeView
        {
            get
            {
                return treeView;
            }
        }

        internal CVScrollbar ScrollBar
        {
            get
            {
                return scrollBar;
            }
            set
            {
                scrollBar = value;
                scrollBar.Scroll += new CVScrollbar.DnVScrollEventHandler(scrollBar_Scroll);
            }
        }

        public CTreeViewNodeCollection Nodes
        {
            get
            {
                return nodes;
            }
        }

        void scrollBar_Scroll(object sender, CVScrollEventArgs e)
        {
            this.Refresh();
        }

        protected void OnNodeMouseClick(object sender, CTreeNodeMouseClickEventArgs e)
        {
            if (NodeMouseClick != null)
            {
                NodeMouseClick(sender, e);
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Point clickedPoint = new Point(e.X, e.Y + scrollBar.Pos);
            if (firstDisplayedNode != null)
            {
                CTreeViewNode foundNode = FindNodeAt(firstDisplayedNode, clickedPoint);
                if (foundNode != null)
                {
                    foundNode.Expanded = (foundNode.Expanded) ? false : true;
                    this.Refresh();

                    CTreeNodeMouseClickEventArgs args = new CTreeNodeMouseClickEventArgs();
                    args.Node = foundNode;
                    OnNodeMouseClick(this, args);
                }
            }
            base.OnMouseClick(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            this.Focus();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (scrollBar.Pos != 0)
                {
                    scrollBar.Pos -= TreeView.NodeHeight;
                    this.Refresh();
                }
            }
            else
            {
                if (scrollBar.Pos != scrollBar.Max)
                {
                    scrollBar.Pos += TreeView.NodeHeight;
                    this.Refresh();
                }
            }
            base.OnMouseWheel(e);
        }

        public override void Refresh()
        {
            firstDisplayedNode = null;
            base.Refresh();
        }

        private CTreeViewNode FindChildNode(CTreeViewNode node, Point p, ref int nodesSearched)
        {
            CTreeViewNode foundNode = null;
            nodesSearched++;
            if (node.Bounds.Contains(p))
            {
                foundNode = node;
            }
            else
            {
                if (node.Expanded)
                {
                    for (int nNode = 0; nNode < node.Nodes.Count; nNode++)
                    {
                        foundNode = FindChildNode(node.Nodes[nNode], p, ref nodesSearched);
                        if (foundNode != null)
                            break;
                    }
                }
            }
            return foundNode;
        }

        private CTreeViewNode FindNextNode(CTreeViewNode node)
        {
            CTreeViewNode nextNode = null;
            CTreeViewNode parentNode = node.Parent;
            if (parentNode == null)
            {
                int nodeIndex = this.Nodes.IndexOf(node);
                if (this.Nodes.Count > nodeIndex + 1)
                    nextNode = this.Nodes[nodeIndex + 1];
            }
            else
            {
                int nodeIndex = parentNode.Nodes.IndexOf(node);
                if (parentNode.Nodes.Count > nodeIndex + 1)
                    nextNode = parentNode.Nodes[nodeIndex + 1];
                else
                    nextNode = FindNextNode(parentNode);
            }
            return nextNode;
        }

        private CTreeViewNode FindNodeAt(CTreeViewNode node, Point p)
        {
            CTreeViewNode foundNode = null;
            CTreeViewNode currentNode = node;
            int nodesSearched = 0;
            while (currentNode != null)
            {

                foundNode = FindChildNode(currentNode, p, ref nodesSearched);
                if (foundNode == null)
                    currentNode = FindNextNode(currentNode);
                else
                    break;
            }
            System.Diagnostics.Debug.WriteLine(String.Format("Node text clicked: {0}", (foundNode != null) ? foundNode.Text : "null"));
            return foundNode;
        }

        int displayedNodes = 0;
        int visibleNodes = 0;

        private void DrawRecursive(Graphics g, CTreeViewNode node, ref Rectangle nodeRect, Rectangle clipRect, int depth = 0)
        {
            if (clipRect.IntersectsWith(nodeRect))
            {
                displayedNodes++;
                if (firstDisplayedNode == null)
                {
                    firstDisplayedNode = node;
                    System.Diagnostics.Debug.WriteLine(String.Format("First Node: {0}", node.Text));
                }
                int indent = TreeView.Indent * depth;
                Rectangle imageRect = new Rectangle(nodeRect.X + indent, nodeRect.Y + ((nodeRect.Height - 16) >> 1), 16, 16);
                if (node.Nodes.Count > 0)
                {
                    if (node.Expanded)
                        g.DrawImageUnscaled(Resources.triangle_expanded_wt, imageRect);
                    else
                        g.DrawImageUnscaled(Resources.triangle_contracted_wt, imageRect);
                }
                Color foreColor = (node.ForeColor == Color.Empty) ? TreeView.ForeColor : node.ForeColor;
                Rectangle textRect = new Rectangle(nodeRect.X + imageRect.Width + indent, nodeRect.Y, nodeRect.Width - imageRect.Width - indent, nodeRect.Height);
                TextRenderer.DrawText(g, node.Text, this.Font, textRect, foreColor,
                    TextFormatFlags.Default | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis | TextFormatFlags.PreserveGraphicsTranslateTransform);
                node.Bounds = textRect;
            }
            visibleNodes++;

            if (node.Expanded)
            {
                int _depth = depth + 1;
                for (int nNode = 0; nNode < node.Nodes.Count; nNode++)
                {
                    nodeRect.Offset(0, TreeView.NodeHeight);
                    DrawRecursive(g, node.Nodes[nNode], ref nodeRect, clipRect, _depth);
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //     #if DEBUG
            //    System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            //     stopWatch.Start();
            //     #endif
            displayedNodes = 0;
            visibleNodes = 0;

            e.Graphics.Clear(this.BackColor);
            e.Graphics.TranslateTransform(0, -scrollBar.Pos);
            Rectangle clipRect = e.ClipRectangle;
            clipRect.Offset(0, scrollBar.Pos);
            Rectangle itemRect = new Rectangle(0, 0, this.ClientSize.Width, TreeView.NodeHeight);
            for (int nNode = 0; nNode < nodes.Count; nNode++)
            {
                DrawRecursive(e.Graphics, this.nodes[nNode], ref itemRect, clipRect);
                itemRect.Offset(0, TreeView.NodeHeight);
            }
            System.Diagnostics.Debug.WriteLine(String.Format("Displayed nodes: {0}", displayedNodes));
            System.Diagnostics.Debug.WriteLine(String.Format("Visible nodes: {0}", visibleNodes));
            int scrollHeight = visibleNodes * TreeView.NodeHeight;
            scrollBar.Max = (scrollHeight <= this.ClientSize.Height) ? 0 : scrollHeight - this.ClientSize.Height;
            base.OnPaint(e);

            //     #if DEBUG
            //      stopWatch.Stop();
            //     TimeSpan ts = stopWatch.Elapsed;
            //     System.Diagnostics.Debug.WriteLine("DnTreeViewNodeContainer draw time (ms) : {0}", ts.TotalMilliseconds);
            //     #endif
        }

        protected override void Dispose(bool disposing)
        {
            this.scrollBar.Scroll -= scrollBar_Scroll;
            base.Dispose(disposing);
        }
    }
}
