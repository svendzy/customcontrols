﻿using System;

namespace CustomControls
{
    public class CTabPageEventArgs : System.EventArgs
    {
        private CTabViewPage tabPage;
        private int tabPageIndex;

        public CTabViewPage TabPage
        {
            get { return tabPage; }
            set { tabPage = value; }
        }

        public int TabPageIndex
        {
            get { return tabPageIndex; }
            set { tabPageIndex = value; }
        }
    }
}
