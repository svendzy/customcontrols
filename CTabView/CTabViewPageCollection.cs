﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomControls
{
    public class CTabViewPageCollection : IList<CTabViewPage>
    {
        private List<CTabViewPage> pages;
        private CTabView tabView;

        public CTabViewPageCollection(CTabView tabView)
        {
            this.tabView = tabView; 
            pages = new List<CTabViewPage>();
        }

        public int IndexOf(CTabViewPage item)
        {
            return pages.IndexOf(item);
        }

        public int IndexOf(string tabPageName)
        {
            int tabPageIndex = -1;
            for (int n = 0; n < pages.Count; n++)
            {
                if (pages[n].Name.Equals(tabPageName))
                {
                    tabPageIndex = n;
                    break;
                }
            }
            return tabPageIndex;
        }

        public void Insert(int index, CTabViewPage item)
        {
            pages.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            pages.RemoveAt(index);
        }

        public CTabViewPage this[int index]
        {
            get
            {
                return pages[index];
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Add(CTabViewPage item)
        {
            pages.Add(item);
        }

        public void Clear()
        {
            pages.Clear();
        }

        public bool Contains(CTabViewPage item)
        {
            return pages.Contains(item);
        }

        public void CopyTo(CTabViewPage[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return pages.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(CTabViewPage item)
        {
            return pages.Remove(item);
        }

        public IEnumerator<CTabViewPage> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
