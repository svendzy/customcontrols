﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;

namespace CustomControls
{
    public class CTabView : Control
    {
        private int headerHeight = 20;
        private int headerColumnWidth = 50;
        private bool loaded = false;

        private CTabViewPageCollection pages;
        private CTabViewPageContainer pageContainer;
        public delegate void TabSelectedEventHandler(object sender, CTabPageEventArgs e);
        public delegate void TabSelectingEventHandler(object sender, CTabPageCancelEventArgs e);
        public event TabSelectedEventHandler TabSelected;
        public event TabSelectingEventHandler TabSelecting;
        private Color gradientStart;
        private Color gradientEnd;

        public CTabView()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            pages = new CTabViewPageCollection(this);
            this.SuspendLayout();
            pageContainer = new CTabViewPageContainer();
            CalculatePositionsAndSize();
            this.Controls.Add(pageContainer);
            this.Theme = CTabViewTheme.Dark;
            this.ResumeLayout(false);
        }

        protected override Size DefaultSize
        {
            get
            {
                return new Size(300, 200);
            }
        }

        public int HeaderHeight
        {
            get { return headerHeight; }
            set
            {
                headerHeight = value;
                if (loaded)
                {
                    PerformLayout(this, "Bounds");
                    this.Invalidate(false);
                    this.Update();
                }
            }
        }

        public int HeaderColumnWidth
        {
            get { return headerColumnWidth; }
            set
            {
                headerColumnWidth = value;
                if (loaded)
                {
                    this.Invalidate(false);
                    this.Update();
                }
            }
        }

        public Color GradientStart
        {
            get { return gradientStart; }
            set { gradientStart = value; }
        }

        public Color GradientEnd
        {
            get { return gradientEnd; }
            set { gradientEnd = value; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public CTabViewPage ActiveTabPage
        {
            get
            {
                return pageContainer.ActiveTabPage;
            }
            set
            {
                pageContainer.ActiveTabPage = value;
            }
        }

        public void SelectTabPage(string tabPageName)
        {
            int tabPageIndex = pages.IndexOf(tabPageName);
            if (tabPageIndex != -1)
            {
                pageContainer.ActiveTabPage = pages[tabPageIndex];
            }
        }

        public CTabViewPageCollection Pages
        {
            get { return pages; }
        }

        internal CTabViewPageContainer PageContainer
        {
            get { return pageContainer; }
        }

        private void CalculatePositionsAndSize()
        {
            pageContainer.Location = new Point(0, headerHeight);
            pageContainer.Size = new Size(this.ClientSize.Width, this.ClientSize.Height - headerHeight);
        }

        public CTabViewTheme Theme
        {
            set
            {
                switch (value)
                {
                    case CTabViewTheme.Dark:
                        this.BackColor = Color.FromArgb(23, 24, 27);
                        this.ForeColor = Color.White;
                        this.pageContainer.BackColor = this.BackColor;
                        this.pageContainer.ForeColor = this.ForeColor;
                        this.gradientStart = Color.FromArgb(33, 33, 33);
                        this.gradientEnd = Color.Black;
                        break;
                    case CTabViewTheme.Light:
                        this.BackColor = Color.White;
                        this.ForeColor = Color.Black;
                        this.pageContainer.BackColor = this.BackColor;
                        this.pageContainer.ForeColor = this.ForeColor;
                        this.gradientStart = Color.FromArgb(248, 248, 248);
                        this.gradientEnd = Color.White;
                        break;
                }
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Rectangle headerRect = new Rectangle(0, 0, this.ClientSize.Width, headerHeight);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Rectangle headerColumnRect = new Rectangle(0, 0, headerColumnWidth, headerHeight);
                for (int nPage = 0; nPage < this.pages.Count; nPage++)
                {
                    if (headerColumnRect.Contains(e.Location))
                    {
                        int tabPageIndex = this.pages.IndexOf(this.pages[nPage]);
                        CTabPageCancelEventArgs tabPageCancelEventArgs = new CTabPageCancelEventArgs();
                        tabPageCancelEventArgs.TabPage = this.pages[nPage];
                        tabPageCancelEventArgs.TabPageIndex = tabPageIndex;
                        tabPageCancelEventArgs.Cancel = false;
                        OnTabSelecting(this, tabPageCancelEventArgs);
                        if (!tabPageCancelEventArgs.Cancel)
                        {
                            this.ActiveTabPage = this.pages[nPage];
                            this.Invalidate(headerRect, false);
                            this.Update();

                            CTabPageEventArgs tabPageEventArgs = new CTabPageEventArgs();
                            tabPageEventArgs.TabPage = this.pages[nPage];
                            tabPageEventArgs.TabPageIndex = tabPageIndex;
                            OnTabSelected(this, tabPageEventArgs);
                        }
                    }
                    headerColumnRect.Offset(headerColumnWidth, 0);
                }
            }
            base.OnMouseClick(e);
        }

        protected void OnTabSelecting(object sender, CTabPageCancelEventArgs e)
        {
            if (TabSelecting != null)
            {
                TabSelecting(sender, e);
            }
        }

        protected void OnTabSelected(object sender, CTabPageEventArgs e)
        {
            if (TabSelected != null)
            {
                TabSelected(sender, e);
            }
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            if ((levent.AffectedControl == this) && (levent.AffectedProperty == "Bounds"))
            {
                CalculatePositionsAndSize();
            }
            base.OnLayout(levent);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle headerRect = new Rectangle(0, 0, this.ClientSize.Width, headerHeight);
            using (SolidBrush backBrush = new SolidBrush(this.BackColor))
            {
                e.Graphics.FillRectangle(backBrush, headerRect);
            }

            Rectangle headerColumnRect = new Rectangle(0, 0, headerColumnWidth, headerHeight);
            int activeTabPageIndex = (ActiveTabPage != null) ? this.pages.IndexOf(ActiveTabPage) : -1;
            for (int nPage = 0; nPage < pages.Count; nPage++)
            {
                using (LinearGradientBrush brush = new LinearGradientBrush(headerColumnRect, gradientStart, gradientEnd, 90F))
                {
                    e.Graphics.FillRectangle(brush, headerColumnRect);
                }

                if (activeTabPageIndex == nPage)
                    ControlPaint.DrawBorder(e.Graphics, headerColumnRect,
                    Color.DarkGray, 1, ButtonBorderStyle.Inset, Color.DarkGray, 1, ButtonBorderStyle.Inset,
                    Color.DarkGray, 1, ButtonBorderStyle.Inset, Color.DarkGray, 1, ButtonBorderStyle.Inset);
                else
                    ControlPaint.DrawBorder(e.Graphics, headerColumnRect,
                    Color.DarkGray, 1, ButtonBorderStyle.Outset, Color.DarkGray, 1, ButtonBorderStyle.Outset,
                    Color.DarkGray, 1, ButtonBorderStyle.Outset, Color.DarkGray, 1, ButtonBorderStyle.Outset);
                Rectangle textRect = headerColumnRect;
                textRect.Offset(-2, -2);
                TextRenderer.DrawText(e.Graphics, pages[nPage].Text, this.Font, textRect, this.ForeColor, TextFormatFlags.Default | TextFormatFlags.EndEllipsis | TextFormatFlags.VerticalCenter | TextFormatFlags.LeftAndRightPadding);
                headerColumnRect.Offset(headerColumnWidth - 1, 0);
            }
            base.OnPaint(e);
        }

    }
}
