﻿using System;

namespace CustomControls
{
    public class CTabPageCancelEventArgs : System.EventArgs
    {
        private CTabViewPage tabPage;
        private int tabPageIndex;
        private bool cancel;

        public CTabViewPage TabPage
        {
            get { return tabPage; }
            set { tabPage = value; }
        }

        public int TabPageIndex
        {
            get { return tabPageIndex; }
            set { tabPageIndex = value; }
        }

        public bool Cancel
        {
            get { return cancel; }
            set { cancel = value; }
        }
    }
}
