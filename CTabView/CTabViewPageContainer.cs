﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CTabViewPageContainer : Control
    {
        private CTabViewPage activeTabPage;

        public CTabViewPageContainer()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.activeTabPage = null;
        }

        public CTabViewPage ActiveTabPage
        {
            get
            {
                return activeTabPage;
            }
            set
            {
                if (activeTabPage != value)
                {
                    if (activeTabPage != null)
                        activeTabPage.Parent = null;
                    activeTabPage = value;
                    activeTabPage.Size = this.ClientSize;
                    activeTabPage.Parent = this;
                }
            }
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            if ((levent.AffectedControl == this) && (levent.AffectedProperty == "Bounds"))
            {
                if (activeTabPage != null)
                {
                    activeTabPage.Size = this.ClientSize;
                }
            }
            base.OnLayout(levent);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(this.BackColor);
            base.OnPaint(e);
        }
    }
}
