﻿using System;

namespace CustomControls
{
    public delegate void ColumnHeaderClickedEventHandler(object sender, ColumnHeaderClickedEventArgs args);

    public class ColumnHeaderClickedEventArgs
    {
        public int ColumnIndex
        { 
            get;
            set;
        }

        public CGridViewColumn Column
        { 
            get;
            set;
        }
    }
}
