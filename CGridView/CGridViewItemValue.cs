﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public abstract class CGridViewItemValue
    {
        private Color textColor;
        private Color backColor;
        private Font font;
        private Rectangle bounds;
        private object tag;
        private ButtonBorderStyle borderStyle;
        private Color borderColor;

        public CGridViewItemValue()
        {
            textColor = DefaultValues.TextColor;
            backColor = DefaultValues.BackColor;
            bounds = Rectangle.Empty;
            font = SystemFonts.DefaultFont;
            tag = null;
            borderStyle = DefaultValues.ItemBorderStyle;
            borderColor = DefaultValues.ItemBorderColor;
        }

        public Color TextColor
        {
            get { return textColor; }
            set { textColor = value; }
        }

        public Color BackColor
        {
            get { return backColor; }
            set { backColor = value; }
        }

        public ButtonBorderStyle BorderStyle
        {
            get { return borderStyle; }
            set { borderStyle = value; }
        }

        public Color BorderColor
        {
            get { return borderColor; }
            set { borderColor = value; }
        }

        public Font Font
        {
            get { return font; }
            internal set { font = value; }
        }

        internal Rectangle Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        internal abstract void Draw(Graphics g);
    }
}
