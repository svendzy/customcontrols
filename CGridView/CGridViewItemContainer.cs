﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewItemContainer : Control
    {
        private CGridViewItemCollection items;
        private CGridView owner;
        internal event ItemClickedEventHandler ItemClicked;
        private CGridViewItem firstDisplayedItem;
        private CVScrollbar scrollBar;
        private int visibleItemCount = 0;

        internal CGridViewItemContainer(CGridView owner)
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.items = new CGridViewItemCollection(this, owner);
            this.owner = owner;
            this.firstDisplayedItem = null;
            this.scrollBar = null;
        }

        public CGridViewItemCollection Items
        {
            get { return items; }
            set { items = value; }
        }

        internal CVScrollbar Scrollbar
        {
            get
            {
                return scrollBar;
            }
            set
            {
                scrollBar = value;
                scrollBar.Scroll += new CVScrollbar.DnVScrollEventHandler(scrollBar_Scroll);
            }
        }

        void scrollBar_Scroll(object sender, CVScrollEventArgs e)
        {
            this.Refresh();
        }

        protected void OnItemClicked(ItemClickedEventArgs args)
        {
            if (ItemClicked != null)
            {
                ItemClicked(this, args);
            }
        }

        private int FindColumnClickedIndex(int x)
        {
            int columnIndex = -1;
            for (int nColumn = 0; nColumn < owner.Columns.Count; nColumn++)
            {
                if (owner.Columns[nColumn].Bounds.Contains(new Point(x, 0)))
                {
                    columnIndex = nColumn;
                    break;
                }
            }
            return columnIndex;
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            Point clickedPoint = new Point(e.X, e.Y + scrollBar.Pos);
            int columnIndex = FindColumnClickedIndex(clickedPoint.X);
            if (columnIndex != -1)
            {
                if (firstDisplayedItem != null)
                {
                    int firstDisplayedItemIndex = this.items.IndexOf(firstDisplayedItem);
                    int lastDisplayedItemIndex = firstDisplayedItemIndex + visibleItemCount;
                    for (int nItem = firstDisplayedItemIndex; nItem < lastDisplayedItemIndex; nItem++)
                    {
                        if (items[nItem].Values.Count > columnIndex)
                        {
                            if (items[nItem].Values[columnIndex].Bounds.Contains(clickedPoint))
                            {
                                ItemClickedEventArgs args = new ItemClickedEventArgs();
                                args.ColumnIndex = columnIndex;
                                args.ItemIndex = nItem;
                                args.Item = items[nItem];
                                OnItemClicked(args);
                                break;
                            }
                        }
                    }
                }
            }
            base.OnMouseClick(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            this.Focus();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (e.Delta > 0)
            {
                if (scrollBar.Pos != 0)
                {
                    scrollBar.Pos -= owner.ItemHeight;
                    this.Refresh();
                }
            }
            else
            {
                if (scrollBar.Pos != scrollBar.Max)
                {
                    scrollBar.Pos += owner.ItemHeight;
                    this.Refresh();
                }
            }
        }

        public override void Refresh()
        {
            firstDisplayedItem = null;
            base.Refresh();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            visibleItemCount = 0;

            // Draw Background
            e.Graphics.Clear(this.BackColor);

            // Translate all drawing to graphics surface by specified amount
            e.Graphics.TranslateTransform(0, -scrollBar.Pos);

            // Set ClipRectangle
            Rectangle clipRect = e.ClipRectangle;
            clipRect.Offset(0, scrollBar.Pos);

            // Make sure we have columns and at least one item
            if ((owner.Columns.Count > 0) && (items.Count > 0))
            {
                // Initiate item bounds
                Rectangle itemBounds = new Rectangle(0, 0, this.ClientSize.Width, owner.ItemHeight);
                
                // Iterrate throung items
                for (int nItem = 0; nItem < items.Count; nItem++)
                {
                    // Set item bounds
                    items[nItem].Bounds = itemBounds;

                    // Check if item should be drawn at all
                    if (items[nItem].Bounds.IntersectsWith(clipRect))
                    {
                        visibleItemCount++;
                        if (firstDisplayedItem == null)
                            firstDisplayedItem = items[nItem];

                        // Declare X offset variable
                        int xOffset = 0;

                        // Iterate through columns
                        for (int nColumn = 0; nColumn < owner.Columns.Count; nColumn++)
                        {
                            // Check if item has value for nColumn
                            if (items[nItem].Values.Count > nColumn)
                            {
                                // Set item values bounds
                                items[nItem].Values[nColumn].Bounds = new Rectangle(xOffset, itemBounds.Y, owner.Columns[nColumn].Width, owner.ItemHeight);

                                // Draw item if inside clip rectangle
                                if (items[nItem].Values[nColumn].Bounds.IntersectsWith(clipRect))
                                {
                                    items[nItem].Values[nColumn].Draw(e.Graphics);
                                }
                            }

                            // Increase X offset
                            xOffset += owner.Columns[nColumn].Width;
                        }
                    }

                    // Offset item bounds by item height
                    itemBounds.Offset(0, owner.ItemHeight);
                }
            }

            int scrollHeight = items.Count * owner.ItemHeight;
            scrollBar.Max = (scrollHeight <= this.ClientSize.Height) ? 0 : scrollHeight - this.ClientSize.Height;
            base.OnPaint(e);
        }
    }
}
