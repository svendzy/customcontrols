﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewColumnHeader : Control
    {
        private CGridViewColumnCollection columns;
        private int cellPadding;
        public event ColumnHeaderClickedEventHandler ColumnHeaderClicked;

        public CGridViewColumnHeader(CGridView owner)
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.columns = new CGridViewColumnCollection(this, owner);
        }

        public CGridViewColumnCollection Columns
        {
            get { return columns; }
            set { columns = value; }
        }

        public int CellPadding
        {
            get { return cellPadding; }
            set { cellPadding = value; }
        }

        protected void OnColumnHeaderClicked(object sender, ColumnHeaderClickedEventArgs args)
        {
            if (ColumnHeaderClicked != null)
            {
                ColumnHeaderClicked(sender, args);
            }
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            for (int nColumn = 0; nColumn < columns.Count; nColumn++)
            {
                if (columns[nColumn].Bounds.Contains(e.Location))
                {
                    ColumnHeaderClickedEventArgs args = new ColumnHeaderClickedEventArgs();
                    args.ColumnIndex = nColumn;
                    args.Column = columns[nColumn];
                    OnColumnHeaderClicked(this, args);
                    break;
                }
            }
            base.OnMouseClick(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(this.BackColor);
            if (columns.Count > 0)
            {
                for (int nColumn = 0; nColumn < columns.Count; nColumn++)
                {
                    using (SolidBrush backBrush = new SolidBrush(columns[nColumn].BackColor))
                    {
                        e.Graphics.FillRectangle(backBrush, columns[nColumn].Bounds);
                    }
                    ControlPaint.DrawBorder(e.Graphics, columns[nColumn].Bounds,
                        columns[nColumn].BorderColor, 1, columns[nColumn].BorderStyle,
                        columns[nColumn].BorderColor, 1, columns[nColumn].BorderStyle,
                        columns[nColumn].BorderColor, 1, columns[nColumn].BorderStyle,
                        columns[nColumn].BorderColor, 1, columns[nColumn].BorderStyle
                        );
                    using (SolidBrush foreBrush = new SolidBrush(columns[nColumn].TextColor))
                    {
                        Rectangle _bounds = columns[nColumn].Bounds;
                        _bounds.Inflate(-cellPadding, -cellPadding);
                        using (StringFormat sf = new StringFormat())
                        {
                            sf.Alignment = StringAlignment.Near;
                            sf.FormatFlags = StringFormatFlags.NoWrap;
                            sf.LineAlignment = StringAlignment.Near;
                            sf.Trimming = StringTrimming.EllipsisCharacter;
                            e.Graphics.DrawString(columns[nColumn].Text, this.Font, foreBrush, _bounds, sf);
                        }
                    }
                }
            }
            base.OnPaint(e);
        }

    }
}
