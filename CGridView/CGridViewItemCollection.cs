﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewItemCollection : IList<CGridViewItem>
    {
        private List<CGridViewItem> items;
        private CGridViewItemContainer owner;
        private CGridView gridView;

        public CGridViewItemCollection(CGridViewItemContainer owner, CGridView gridView)
        {
            this.items = new List<CGridViewItem>();
            this.owner = owner;
            this.gridView = gridView;
        }

        public int IndexOf(CGridViewItem item)
        {
            return items.IndexOf(item);
        }

        public void Insert(int index, CGridViewItem item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public CGridViewItem this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                items[index] = value;
            }
        }

        public void Add(CGridViewItem item)
        {
            items.Add(item);
            if ((gridView != null) && (!gridView.Updating))
                gridView.ItemContainer.Refresh();
        }

        public void Clear()
        {
            items.Clear();
            if ((gridView != null) && (!gridView.Updating))
                gridView.ItemContainer.Refresh();
        }

        public bool Contains(CGridViewItem item)
        {
            return items.Contains(item);
        }

        public void CopyTo(CGridViewItem[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(CGridViewItem item)
        {
            bool bSuccess = items.Remove(item);
            if (bSuccess)
            {
                if ((gridView != null) && (!gridView.Updating))
                    gridView.ItemContainer.Refresh();
            }
            return bSuccess;
        }

        public IEnumerator<CGridViewItem> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public CGridViewItem FindByName(string name)
        {
            CGridViewItem item = null;
            for (int n = 0; n < items.Count; n++)
            {
                if (items[n].Name.Equals(name))
                {
                    item = items[n];
                    break;
                }
            }
            return item;
        }
    }
}
