﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace CustomControls
{
    internal class StringFormats : IDisposable
    {
        [ThreadStatic]
        private static StringFormats _current = null;
        private bool isDisposed = false;

        public enum FormatFlag
        {
            LeftMiddle = 1,
            CenterMiddle = 2,
            RightMiddle = 4,
            LeftTop = 8,
            CenterTop = 16,
            RightTop = 32,
            LeftBottom = 64,
            CenterBottom = 128,
            RightBottom = 256,
            Ellipsis = 512,
            NoWrap = 1024
        }
        private readonly Dictionary<FormatFlag, StringFormat> _stringFormats = new Dictionary<FormatFlag, StringFormat>();

        internal StringFormats()
        {
            if (_current == null)
                _current = this;
        }

        public bool IsDisposed
        {
            get { return isDisposed; }
        }

        internal static StringFormat GetStringFormat(FormatFlag format)
        {
            if (!_current._stringFormats.ContainsKey(format))
            {
                StringFormat sf = new StringFormat();
                switch (format)
                {
                    case FormatFlag.LeftMiddle:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Center;
                        break;
                    case FormatFlag.CenterMiddle:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        break;
                    case FormatFlag.RightMiddle:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Center;
                        break;
                    case FormatFlag.LeftTop:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Near;
                        break;
                    case FormatFlag.CenterTop:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Near;
                        break;
                    case FormatFlag.RightTop:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Near;
                        break;
                    case FormatFlag.LeftBottom:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Far;
                        break;
                    case FormatFlag.CenterBottom:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Far;
                        break;
                    case FormatFlag.RightBottom:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Far;
                        break;
                    case FormatFlag.LeftMiddle | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.CenterMiddle | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.RightMiddle | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.LeftTop | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.CenterTop | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.RightTop | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.LeftBottom | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.CenterBottom | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.RightBottom | FormatFlag.Ellipsis:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        break;
                    case FormatFlag.LeftMiddle | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.CenterMiddle | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.RightMiddle | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Center;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.LeftTop | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.CenterTop | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.RightTop | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Near;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.LeftBottom | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Near;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.CenterBottom | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    case FormatFlag.RightBottom | FormatFlag.Ellipsis | FormatFlag.NoWrap:
                        sf.Alignment = StringAlignment.Far;
                        sf.LineAlignment = StringAlignment.Far;
                        sf.Trimming = StringTrimming.EllipsisCharacter;
                        sf.FormatFlags = StringFormatFlags.NoWrap;
                        break;
                    default:
                        throw new FormatException("Invalid StringFormat specified");
                }
                _current._stringFormats[format] = sf;
            }
            return _current._stringFormats[format];
        }

        public void Dispose()
        {
            if (_current == this) _current = null;
            foreach (StringFormat stringFormat in _stringFormats.Values) stringFormat.Dispose();
            isDisposed = true;
        }
    }
}
