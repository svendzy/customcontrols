﻿using System;

namespace CustomControls
{
    public delegate void ItemClickedEventHandler(object sender, ItemClickedEventArgs args);

    public class ItemClickedEventArgs : System.EventArgs
    {
        public int ColumnIndex 
        { 
            get; 
            set; 
        }

        public int ItemIndex 
        { 
            get; 
            set; 
        }

        public CGridViewItem Item 
        { 
            get; 
            set; 
        }
    }
}
