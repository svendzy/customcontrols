﻿using System;
using System.Collections.Generic;

namespace CustomControls
{
    public class CGridViewItemValueCollection : IList<CGridViewItemValue>
    {
        private List<CGridViewItemValue> items;
        private CGridViewItem listViewItem;

        public CGridViewItemValueCollection(CGridViewItem listViewItem)
        {
            this.items = new List<CGridViewItemValue>();
            this.listViewItem = listViewItem;
        }

        public int IndexOf(CGridViewItemValue item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, CGridViewItemValue item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public CGridViewItemValue this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                items[index] = value;
            }
        }

        public void Add(CGridViewItemValue item)
        {
            items.Add(item);
        }

        public void Clear()
        {
            items.Clear();
        }

        public bool Contains(CGridViewItemValue item)
        {
            return items.Contains(item);
        }

        public void CopyTo(CGridViewItemValue[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return items.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(CGridViewItemValue item)
        {
            return items.Remove(item);
        }

        public IEnumerator<CGridViewItemValue> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
