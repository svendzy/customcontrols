﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewItem
    {
        private string name;
        private CGridViewItemValueCollection values;
        private object tag;
        private Rectangle bounds;
        private int cellPadding;

        public CGridViewItem()
        {
            this.name = String.Empty;
            this.values = new CGridViewItemValueCollection(this);
            this.tag = null;
            this.bounds = Rectangle.Empty;
            this.cellPadding = DefaultValues.CellPadding;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        internal Rectangle Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

        public CGridViewItemValueCollection Values
        {
            get { return values; }
        }

        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        internal int CellPadding
        {
            get { return cellPadding; }
            set { cellPadding = value; }
        }
    }
}
