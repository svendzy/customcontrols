﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewPriceSizeItemValue : CGridViewItemValue
    {
        private double price;
        private double size;

        public CGridViewPriceSizeItemValue()
            : base()
        {
            price = 0;
            size = 0;
        }

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public double Size
        {
            get { return size; }
            set { size = value; }
        }

        internal override void Draw(System.Drawing.Graphics g)
        {
            Font boldFont = GraphicsCache.GetFont(FontStyle.Bold);
            StringFormat centerTopAlign = StringFormats.GetStringFormat(StringFormats.FormatFlag.CenterTop);
            StringFormat centerBottomAlign = StringFormats.GetStringFormat(StringFormats.FormatFlag.CenterBottom);
            SolidBrush backBrush = GraphicsCache.GetSolidBrush(BackColor);
            g.FillRectangle(backBrush, Bounds);
            ControlPaint.DrawBorder(g, Bounds, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle);
            if (price > 0)
            {
                SolidBrush textBrush = GraphicsCache.GetSolidBrush(TextColor);
                g.DrawString(price.ToString(), boldFont, textBrush, Bounds, centerTopAlign);
                int _size = Convert.ToInt32(size);
                g.DrawString(_size.ToString(), Font, textBrush, Bounds, centerBottomAlign);
            }
        }
    }
}
