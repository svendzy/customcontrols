﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace CustomControls
{
    internal class GraphicsCache : IDisposable
    {
        [ThreadStatic]
        private static GraphicsCache _current = null;
        private readonly Dictionary<Color, SolidBrush> _solidBrushes = new Dictionary<Color, SolidBrush>();
        private readonly Dictionary<FontStyle, Font> _fonts = new Dictionary<FontStyle, Font>();
        private bool isDisposed = false;

        internal GraphicsCache()
        {
            if (_current == null)
                _current = this;
        }

        public bool IsDisposed
        {
            get { return isDisposed; }
        }

        public void Dispose()
        {
            if (_current == this) _current = null;
            foreach (SolidBrush solidBrush in _solidBrushes.Values) solidBrush.Dispose();
            foreach (Font font in _fonts.Values) font.Dispose();
            isDisposed = true;
        }

        internal static Font GetFont(FontStyle fontStyle)
        {
            if (!_current._fonts.ContainsKey(fontStyle))
                _current._fonts[fontStyle] = new Font(SystemFonts.DefaultFont, fontStyle);
            return _current._fonts[fontStyle];
        }

        internal static SolidBrush GetSolidBrush(Color color)
        {
            if (!_current._solidBrushes.ContainsKey(color))
                _current._solidBrushes[color] = new SolidBrush(color);

            return _current._solidBrushes[color];
        }
    }
}
