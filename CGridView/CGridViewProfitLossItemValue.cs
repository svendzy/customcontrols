﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewProfitLossItemValue : CGridViewItemValue
    {
        private double value;
        private string currencySymbol;

        public CGridViewProfitLossItemValue()
            : base()
        {
            this.value = 0;
            this.currencySymbol = String.Empty;
        }

        public double Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public string CurrencySymbol
        {
            get { return this.currencySymbol; }
            set { this.currencySymbol = value; }
        }

        internal override void Draw(System.Drawing.Graphics g)
        {
            SolidBrush backBrush = GraphicsCache.GetSolidBrush(BackColor);
            g.FillRectangle(backBrush, Bounds);
            ControlPaint.DrawBorder(g, Bounds, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle);

            string strVal = (String.IsNullOrEmpty(currencySymbol)) ?
                    String.Format("{0:0.00}", value) : currencySymbol + String.Format("{0:0.00}", value);
            if (this.value >= 0)
            {
                g.DrawString(strVal, Font, Brushes.LimeGreen, Bounds);
            }
            else
            {
                g.DrawString(strVal, Font, Brushes.Red, Bounds);
            }
        }
    }
}
