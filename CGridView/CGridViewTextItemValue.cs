﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewTextItemValue : CGridViewItemValue
    {
        private string text;
        public CGridViewTextItemValue()
            : base()
        {
            this.text = String.Empty;
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        internal override void Draw(System.Drawing.Graphics g)
        {
            SolidBrush backBrush = GraphicsCache.GetSolidBrush(BackColor);
            g.FillRectangle(backBrush, Bounds);
            ControlPaint.DrawBorder(g, Bounds, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle, BorderColor, 1, BorderStyle);
            SolidBrush textBrush = GraphicsCache.GetSolidBrush(TextColor);
            g.DrawString(text, Font, textBrush, Bounds);
        }
    }
}
