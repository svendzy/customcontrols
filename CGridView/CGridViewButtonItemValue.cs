﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace CustomControls
{
    public class CGridViewButtonItemValue : CGridViewItemValue
    {
        private string text;
        private Color buttonColor;
        private Color buttonBorderColor;
        private ButtonBorderStyle buttonBorderStyle;

        public CGridViewButtonItemValue()
            : base()
        {
            this.text = String.Empty;
            this.buttonColor = DefaultValues.ButtonColor;
            this.buttonBorderColor = DefaultValues.ButtonBorderColor;
            this.buttonBorderStyle = DefaultValues.ButtonBorderStyle;
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public Color ButtonColor
        {
            get { return buttonColor; }
            set { buttonColor = value; }
        }

        public Color ButtonBorderColor
        {
            get { return buttonBorderColor; }
            set { buttonBorderColor = value; }
        }

        public ButtonBorderStyle ButtonBorderStyle
        {
            get { return buttonBorderStyle; }
            set { buttonBorderStyle = value; }
        }

        internal override void Draw(Graphics g)
        {
            SolidBrush buttonBrush = GraphicsCache.GetSolidBrush(buttonColor);
            g.FillRectangle(buttonBrush, Bounds);
            ControlPaint.DrawBorder(g, Bounds, buttonBorderColor, 1, buttonBorderStyle, buttonBorderColor, 1, buttonBorderStyle,
                    buttonBorderColor, 1, buttonBorderStyle, buttonBorderColor, 1, buttonBorderStyle);
            SolidBrush textBrush = GraphicsCache.GetSolidBrush(TextColor);
            g.DrawString(text, Font, textBrush, Bounds);
        }
    }
}
