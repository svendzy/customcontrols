﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewColumnCollection : IList<CGridViewColumn>
    {
        private List<CGridViewColumn> columns;
        private CGridViewColumnHeader owner;
        private CGridView gridView;

        public CGridViewColumnCollection(CGridViewColumnHeader owner, CGridView gridView)
        {
            this.columns = new List<CGridViewColumn>();
            this.owner = owner;
            this.gridView = gridView;
        }

        private void CalculateColumnBounds()
        {
            int offsetX = 0;
            for (int nColumn = 0; nColumn < columns.Count; nColumn++)
            {
                Rectangle bounds = new Rectangle(offsetX, 0, columns[nColumn].Width, DefaultValues.ColumnHeaderHeight);
                columns[nColumn].Bounds = bounds;
                offsetX += bounds.Width;
            }
        }

        public int IndexOf(CGridViewColumn item)
        {
            return columns.IndexOf(item);
        }

        public void Insert(int index, CGridViewColumn item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public CGridViewColumn this[int index]
        {
            get
            {
                return columns[index];
            }
            set
            {
                columns[index] = value;
            }
        }

        public void Add(CGridViewColumn item)
        {
            columns.Add(item);
            CalculateColumnBounds();
            if ((gridView != null) && (!gridView.Updating))
                gridView.ColumnHeader.Refresh();
        }

        public void Clear()
        {
            columns.Clear();
            if ((gridView != null) && (!gridView.Updating))
                gridView.ColumnHeader.Refresh();
        }

        public bool Contains(CGridViewColumn item)
        {
            return columns.Contains(item);
        }

        public void CopyTo(CGridViewColumn[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return columns.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(CGridViewColumn item)
        {
            bool bSuccess = columns.Remove(item);
            if (bSuccess)
            {
                CalculateColumnBounds();
                if ((gridView != null) && (!gridView.Updating))
                    gridView.ColumnHeader.Refresh();
            }
            return bSuccess;
        }

        public IEnumerator<CGridViewColumn> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
