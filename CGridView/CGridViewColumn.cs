﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridViewColumn
    {
        private Color textColor;
        private Color backColor;
        private Color borderColor;
        private ButtonBorderStyle borderStyle;
        private Rectangle bounds;
        private string text;
        private int width;

        public CGridViewColumn()
        {
            this.textColor = DefaultValues.TextColor;
            this.backColor = DefaultValues.BackColor;
            this.borderColor = DefaultValues.ColumnHeaderBorderColor;
            this.borderStyle = DefaultValues.ColumnHeaderBorderStyle;
            this.bounds = new Rectangle();
            this.text = String.Empty;
            this.width = DefaultValues.ColumnWidth;
        }

        public Color BackColor
        {
            get { return backColor; }
            set { backColor = value; }
        }

        public Color TextColor
        {
            get { return textColor; }
            set { textColor = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        public Rectangle Bounds
        {
            get { return bounds; }
            internal set { bounds = value; }
        }

        public Color BorderColor
        {
            get { return borderColor; }
            set { borderColor = value; }
        }

        public ButtonBorderStyle BorderStyle
        {
            get { return borderStyle; }
            set { borderStyle = value; }
        }
    }
}
