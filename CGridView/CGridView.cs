﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    public class CGridView : Control
    {
        private CGridViewColumnHeader columnHeader;
        private CGridViewItemContainer itemContainer;
        private GraphicsCache graphicsCache;
        private StringFormats stringFormats;

        private int cellPadding;
        private int itemHeight;
        private int columnHeaderHeight;

        private bool updating = false;

        public event ItemClickedEventHandler ItemClicked;
        public event ColumnHeaderClickedEventHandler ColumnHeaderClicked;

        public CGridView()
        {
            graphicsCache = new GraphicsCache();
            stringFormats = new StringFormats();

            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, false);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.cellPadding = DefaultValues.CellPadding;
            this.itemHeight = DefaultValues.ItemHeight;
            this.columnHeaderHeight = DefaultValues.ColumnHeaderHeight;
            this.BackColor = DefaultValues.BackColor;
            this.ForeColor = DefaultValues.TextColor;
            this.SuspendLayout();
            columnHeader = new CGridViewColumnHeader(this);
            columnHeader.Location = new Point(0, 0);
            columnHeader.Size = new Size(this.ClientSize.Width, this.columnHeaderHeight);

            CVScrollbar scrollBar = new CVScrollbar();
            scrollBar.Location = new Point(this.ClientSize.Width - 16, this.columnHeaderHeight);
            scrollBar.Size = new Size(16, this.ClientSize.Height - this.columnHeaderHeight);
            scrollBar.AutoScrollMin = this.ClientSize.Height - this.columnHeaderHeight;
            scrollBar.Max = scrollBar.AutoScrollMin;
            scrollBar.SmallChange = itemHeight;

            itemContainer = new CGridViewItemContainer(this);
            itemContainer.Location = new Point(0, this.columnHeaderHeight);
            itemContainer.Size = new Size(this.ClientSize.Width - 16, this.ClientSize.Height - this.columnHeaderHeight);
            itemContainer.Scrollbar = scrollBar;

            this.Controls.Add(columnHeader);
            this.Controls.Add(itemContainer);
            this.Controls.Add(scrollBar);
            this.ResumeLayout();

            columnHeader.ColumnHeaderClicked +=
                new ColumnHeaderClickedEventHandler(columnHeader_ColumnHeaderClicked);
            itemContainer.ItemClicked +=
                new ItemClickedEventHandler(itemContainer_ItemClicked);
        }

        protected override Size DefaultSize
        {
            get
            {
                return new Size(300, 200);
            }
        }

        void columnHeader_ColumnHeaderClicked(object sender, ColumnHeaderClickedEventArgs args)
        {
            OnColumnHeaderClicked(this, args);
        }

        void itemContainer_ItemClicked(object sender, ItemClickedEventArgs args)
        {
            OnItemClicked(this, args);
        }

        protected void OnColumnHeaderClicked(object sender, ColumnHeaderClickedEventArgs args)
        {
            if (ColumnHeaderClicked != null)
            {
                ColumnHeaderClicked(sender, args);
            }
        }

        protected void OnItemClicked(object sender, ItemClickedEventArgs args)
        {
            if (ItemClicked != null)
            {
                ItemClicked(sender, args);
            }
        }

        public CGridViewColumnCollection Columns
        {
            get { return columnHeader.Columns; }
        }

        public CGridViewItemCollection Items
        {
            get { return itemContainer.Items; }
        }

        public int CellPadding
        {
            get { return cellPadding; }
        }

        public int ItemHeight
        {
            get
            {
                return itemHeight;
            }
            set
            {
                itemHeight = value;
            }
        }

        public int ColumnHeaderHeight
        {
            get
            {
                return columnHeaderHeight;
            }
            set
            {
                columnHeaderHeight = value;
            }
        }

        public CGridViewColumnHeader ColumnHeader
        {
            get
            {
                return columnHeader;
            }
        }

        public CGridViewItemContainer ItemContainer
        {
            get
            {
                return itemContainer;
            }
        }

        internal bool Updating
        {
            get
            {
                return updating;
            }
        }

        public CGridViewTheme Theme
        {
            set
            {
                switch (value)
                {
                    case CGridViewTheme.Dark:
                        this.BackColor = Color.FromArgb(23, 24, 27);
                        this.ForeColor = Color.White;
                        this.columnHeader.BackColor = Color.Black;
                        this.columnHeader.ForeColor = Color.White;
                        this.itemContainer.BackColor = this.BackColor;
                        this.itemContainer.ForeColor = this.ForeColor;
                        this.itemContainer.Scrollbar.BackColor = Color.FromArgb(48, 51, 60);
                        this.itemContainer.Scrollbar.ForeColor = Color.FromArgb(131, 136, 140);
                        break;
                    case CGridViewTheme.Light:
                        this.BackColor = Color.FromArgb(250, 250, 250);
                        this.ForeColor = Color.Black;
                        this.itemContainer.BackColor = this.BackColor;
                        this.itemContainer.ForeColor = this.ForeColor;
                        this.itemContainer.Scrollbar.BackColor = Color.FromArgb(209, 209, 209);
                        this.itemContainer.Scrollbar.ForeColor = Color.FromArgb(0, 143, 0);
                        break;
                }
            }
        }

        public override void Refresh()
        {
            columnHeader.Refresh();
            itemContainer.Refresh();
        }

        public void BeginUpdate()
        {
            updating = true;
        }

        public void EndUpdate()
        {
            updating = false;
            this.Refresh();
        }

        protected override void OnLayout(LayoutEventArgs levent)
        {
            if ((levent.AffectedControl == this) && (levent.AffectedProperty == "Bounds"))
            {
                columnHeader.Location = new Point(0, 0);
                columnHeader.Size = new Size(this.ClientSize.Width, this.columnHeaderHeight);

                itemContainer.Location = new Point(0, this.columnHeaderHeight);
                itemContainer.Size = new Size(this.ClientSize.Width - 16, this.ClientSize.Height - this.columnHeaderHeight);
                itemContainer.Scrollbar.Location = new Point(this.ClientSize.Width - 16, this.columnHeaderHeight);
                itemContainer.Scrollbar.Size = new Size(16, this.ClientSize.Height - this.columnHeaderHeight);
                itemContainer.Scrollbar.AutoScrollMin = this.ClientSize.Height - this.columnHeaderHeight;
                itemContainer.Scrollbar.Max = itemContainer.Scrollbar.AutoScrollMin;
            }
            base.OnLayout(levent);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        { }

        protected override void OnPaint(PaintEventArgs e) 
        { }

        protected override void Dispose(bool disposing)
        {
            if (!columnHeader.IsDisposed)
            {
                columnHeader.ColumnHeaderClicked -= columnHeader_ColumnHeaderClicked;
                columnHeader.Dispose();
            }
            if (!itemContainer.IsDisposed)
            {
                itemContainer.ItemClicked -= itemContainer_ItemClicked;
                itemContainer.Dispose();
            }
            if (!graphicsCache.IsDisposed)
                graphicsCache.Dispose();
            if (!stringFormats.IsDisposed)
                stringFormats.Dispose();

            base.Dispose(disposing);
        }
    }
}
