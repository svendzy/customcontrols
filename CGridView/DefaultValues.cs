﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace CustomControls
{
    internal static class DefaultValues
    {
        internal static Color BackColor
        {
            get { return Color.Black; }
        }

        internal static Color TextColor
        {
            get { return Color.White; }
        }

        internal static int CellPadding
        {
            get { return 5; }
        }

        internal static int CellSpacing
        {
            get { return 0; }
        }

        internal static ContentAlignment ContentAlignment
        {
            get { return ContentAlignment.TopLeft; }
        }

        internal static ButtonBorderStyle ItemBorderStyle
        {
            get { return ButtonBorderStyle.Outset; }
        }

        internal static Color ItemBorderColor
        {
            get { return Color.SlateGray; }
        }

        internal static int ItemHeight
        {
            get { return 35; }
        }

        internal static int ColumnHeaderHeight
        {
            get { return 30; }
        }

        internal static ButtonBorderStyle ColumnHeaderBorderStyle
        {
            get { return ButtonBorderStyle.Outset; }
        }

        internal static Color ColumnHeaderBorderColor
        {
            get { return Color.SlateGray; }
        }

        internal static int ColumnWidth
        {
            get { return 40; }
        }

        internal static Color ButtonColor
        {
            get { return Color.Black; }
        }

        internal static Color ButtonBorderColor
        {
            get { return Color.SlateGray; }
        }

        internal static ButtonBorderStyle ButtonBorderStyle
        {
            get { return ButtonBorderStyle.Outset; }
        }
    }
}
